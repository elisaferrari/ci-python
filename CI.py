import os
import sys
import numpy

# This is an example monotonicity evaluator using the delta monotonicity from 
# [Brooks, Martin, Yuhong Yan, and Daniel Lemire. "Scale-based monotonicity analysis in qualitative modelling with flat segments." (2005)]
# If you want to use a different function for evaluating monotonicity modify __call__ and evaluate_monotonicity appropriately.
class DeltaMonotonicityEvaluator():
    def __init__(self,thr):
        self.Threshold = thr

    def find_delta_pairs(self,y):
        pairs = []
        for start in range(len(y)): 
            for end in range(start,len(y)):
                (test,val) = self.evaluate_pair(y,start,end)
                if test:
                    pairs.append((start,end,val))
        return pairs

    def evaluate_pair(self,y,start,end):
        diff = y[end]-y[start]
        if abs(diff) > self.Threshold:
            for j in range(start+1,end):
                if abs(y[j]-y[start]) < self.Threshold:
                    pass
                else:
                    return (False,0)
        else:
            return (False,0)
        return (True, numpy.sign(diff))  

    def check_pairs(self,pairs):
        a = numpy.unique([x[2] for x in pairs])
        if len(a)==1:
            return a[0]
        else:
            return 0     
    
    def evaluate_monotonicity(self,aucs):
        pairs = self.find_delta_pairs(aucs)
        return self.check_pairs(pairs) == 1

    def __call__(self,aucs,errs):
        return self.evaluate_monotonicity(aucs)


class CICalculator():

    def __init__(self,monotonicity_evaluator, biases, aucs, errs, biases_s, aucs_s, errs_s):
        # Monotonicity evaluator should be user-defined and take as input a list of aucs and a list of errors
        # It should return a boolean True/False
        self.MonotonicityEvaluator = monotonicity_evaluator
        
        (self.biases,self.aucs,self.errs) = self.sort_by_bias(biases,aucs,errs)
        (self.biases_s,self.aucs_s,self.errs_s) = self.sort_by_bias(biases_s,aucs_s,errs_s)

    def sort_by_bias(self,biases,aucs,errs):
        sorted_biases = numpy.array(numpy.sort(biases))
        sorted_aucs = numpy.array([x for _,x in sorted(zip(biases,aucs))])
        if errs is not None:
            sorted_errs = numpy.array([x for _,x in sorted(zip(biases,errs))])
        else:
            sorted_errs = numpy.array([0]*len(biases))
        return sorted_biases,sorted_aucs,sorted_errs

    def __calculate_integral(self,biases,aucs,errs):
        val = numpy.trapz(aucs,x=biases)
        err = numpy.trapz(errs,x=biases)
        return val,err

    def __evaluate_monotonicity(self,aucs,errs):
        return self.MonotonicityEvaluator(aucs,errs)        

    def __calculate_ci_part(self,biases,aucs,errs):
        biases_neg = [x for x in biases if x<=0]
        aucs_neg   = aucs[:len(biases_neg)]
        errs_neg   = errs[:len(biases_neg)]

        biases_pos = [x for x in biases if x>=0]
        aucs_pos   =   aucs[-len(biases_pos):]
        errs_pos   =   errs[-len(biases_pos):]

        # last auc_neg and first auc_pos are the ones closest to 0 bias (having exactly 0 bias is suggested)
        i_neg,err_neg = self.__calculate_integral(biases_neg,aucs_neg-aucs_neg[-1],errs_neg)        
        i_pos,err_pos = self.__calculate_integral(biases_pos,aucs_pos-aucs_pos[0],errs_pos)
        
        return (i_pos-i_neg, abs(err_neg) + abs(err_pos))

    def calculate_ci(self):
        # psi
        val, err   = self.__calculate_ci_part(self.biases,self.aucs,self.errs)
        monotonicity   = self.__evaluate_monotonicity(self.aucs,self.errs)
        #print(val,err,monotonicity)

        # psi_s
        val_s, err_s = self.__calculate_ci_part(self.biases_s,self.aucs_s,self.errs_s)
        monotonicity_s   = self.__evaluate_monotonicity(self.aucs_s,self.errs_s)
        #print(val_s,err_s,monotonicity_s)

        # this will return 0 if both monotonicities are False, or if integral values are negative
        if monotonicity == 0 and monotonicity_s == 0:
            return (0,0)
        else:
            if monotonicity*val >= monotonicity_s*val_s:
                if val > 0:
                    return val,err
                else:
                    return (0,0)
            else:
                if val_s > 0:
                    return val_s,err_s
                else:
                    return (0,0)


