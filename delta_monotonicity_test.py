import numpy

from CI import DeltaMonotonicityEvaluator

delta = 0.1
dm = DeltaMonotonicityEvaluator(delta)

def test_not_monotone():
    y = [0.1,0.2,0.4,-0.6,1]
    res = dm(y,None)
    print("Result test_not_monotone",res)
    return res == False


def test_monotone():
    y = [0.1,0.2,0.4,0.6,1]
    res = dm(y,None)
    print("Result test_monotone",res)
    return res == True

def test_monotone_2():
    y = [0.1,0.05,0.2,0.4,0.6,1]
    res = dm(y,None)
    print("Result test_monotone_2",res)
    return res == True    

print(test_monotone())
print(test_monotone_2())
print(test_not_monotone())