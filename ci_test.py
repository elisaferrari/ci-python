import os
import sys
import numpy

from CI import DeltaMonotonicityEvaluator,CICalculator


# Initialize monotonicity evaluator
dm = DeltaMonotonicityEvaluator(0.1)

def test_ci():
    numpy.random.seed(0)

    # Generate random biases/aucs
    biases = numpy.arange(-1,1.1,0.2)
    biases[5] = 0.0
    aucs = numpy.sort( numpy.random.rand(len(biases)) )
    errs = 0.1*numpy.array(aucs)

    biases_s = biases
    aucs_s = numpy.sort( numpy.random.rand(len(biases)) )
    errs_s = 0.1*numpy.array(aucs_s)

    c = CICalculator(dm,
        biases,
        aucs,
        errs,
        biases_s,
        aucs_s,
        errs_s
    )
    r = c.calculate_ci()
    print("Result test_ci",r)
    return r[0]==0.5302964659908191

def test_unsorted_ci():
    numpy.random.seed(0)
    biases = numpy.arange(-1,1.1,0.2)
    biases[5] = 0.0
    aucs = numpy.sort( numpy.random.rand(len(biases)) )
    errs = 0.1*numpy.array(aucs)

    biases_s = biases
    aucs_s = numpy.sort( numpy.random.rand(len(biases)) )
    errs_s = 0.1*numpy.array(aucs_s)

    perm = numpy.random.permutation(range(len(biases)))
    biases = biases[perm]
    aucs = aucs[perm]
    errs = errs[perm]

    perm = numpy.random.permutation(range(len(biases)))
    biases_s = biases_s[perm]
    aucs_s = aucs_s[perm]
    errs_s = errs_s[perm]

    c = CICalculator(dm,
        biases,
        aucs,
        errs,
        biases_s,
        aucs_s,
        errs_s
    )
    r = c.calculate_ci()
    print("Result test_unsorted_ci",r)

    return r[0]==0.5302964659908191


def test_best_ci():
    biases = numpy.arange(-1,1.005,0.01)
    biases[100] = 0.0
    aucs = numpy.array([0]*100 + [0.5] + [1.0]*100)
    errs = 0.1*numpy.array(aucs)   

    biases_s = biases
    aucs_s = numpy.array([0]*100 + [0.5] + [1.0]*100)
    errs_s = 0.1*numpy.array(aucs_s)     

    c = CICalculator(dm,
        biases,
        aucs,
        errs,
        biases_s,
        aucs_s,
        errs_s
    )
    r = c.calculate_ci()
    print("Result test_best_ci",r)
    return r[0]>0.99       

def test_translation_invariance_ci():
    numpy.random.seed(0)

    # Generate random biases/aucs
    biases = numpy.arange(-1,1.1,0.2)
    biases[5] = 0.0
    aucs = numpy.sort( numpy.random.rand(len(biases)) )
    errs = 0.1*numpy.array(aucs)

    biases_s = biases
    aucs_s = numpy.sort( numpy.random.rand(len(biases)) )
    errs_s = 0.1*numpy.array(aucs_s)

    aucs = aucs+0.1
    aucs_s = aucs_s+0.1

    c = CICalculator(dm,
        biases,
        aucs,
        errs,
        biases_s,
        aucs_s,
        errs_s
    )
    r = c.calculate_ci()
    print("Result test_translation_invariance_ci",r)
    return r[0]==0.5302964659908191    

def test_null_ci():
    biases = numpy.arange(-1,1.005,0.01)
    biases[100] = 0.0
    aucs = [0.5]*len(biases)
    errs = 0.1*numpy.array(aucs)   

    biases_s = biases
    aucs_s = [0.5]*len(biases)
    errs_s = 0.1*numpy.array(aucs_s)     

    c = CICalculator(dm,
        biases,
        aucs,
        errs,
        biases_s,
        aucs_s,
        errs_s
    )
    r = c.calculate_ci()
    print("Result test_null_ci",r)
    return r[0]==0.0     

def test_inverted_ci():
    biases = numpy.arange(-1,1.005,0.01)
    biases[100] = 0.0
    aucs = numpy.flip(numpy.array([0]*100 + [0.5] + [1.0]*100))
    errs = 0.1*numpy.array(aucs)   

    biases_s = biases
    aucs_s = numpy.flip(numpy.array([0]*100 + [0.5] + [1.0]*100))
    errs_s = 0.1*numpy.array(aucs_s)     

    c = CICalculator(dm,
        biases,
        aucs,
        errs,
        biases_s,
        aucs_s,
        errs_s
    )
    r = c.calculate_ci()
    print("Result test_inverted_ci",r)
    return r[0]== 0.0  

def test_no_errors_ci():
    # just check it does not throw
    try:
        biases = numpy.arange(-1,1.005,0.01)
        biases[100] = 0.0
        aucs = numpy.flip(numpy.array([0]*100 + [0.5] + [1.0]*100))
        errs = None

        biases_s = biases
        aucs_s = numpy.flip(numpy.array([0]*100 + [0.5] + [1.0]*100))
        errs_s = None

        c = CICalculator(dm,
            biases,
            aucs,
            errs,
            biases_s,
            aucs_s,
            errs_s
        )
        r = c.calculate_ci()
        print("Result test_no_errors_ci",r)
        return True
    except:
        return False

    

print(test_ci())
print(test_unsorted_ci())
print(test_best_ci())
print(test_translation_invariance_ci())
print(test_null_ci())
print(test_inverted_ci())
print(test_no_errors_ci())