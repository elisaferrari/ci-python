import os
import sys
import numpy

from CI import DeltaMonotonicityEvaluator,CICalculator

# Initialize monotonicity evaluator
dm = DeltaMonotonicityEvaluator(0.1)

# Set random seed for reproducible results
numpy.random.seed(0)

# Generate random biases/aucs (substitute with real values)
# Biases needs not be sorted (albeit they are here), but should contain a value very close to 0, exactly 0 is better.
# Biases should go from -1 to 1. With -1 complete opposite bias with respect to validation and +1 complete bias in the same way as in validation
# Bias = 0.0 means no bias, i.e., balanced training dataset.
biases = numpy.arange(-1,1.1,0.2)
biases[5] = 0.0

# Errors and AUCS need to be in the same order as the biases
aucs = numpy.sort( numpy.random.rand(len(biases)) )
errs = 0.1*numpy.array(aucs)


# Same thing for the ***_s variables (needed for phi* calculation as described in the paper)
biases_s = biases
aucs_s = numpy.sort( numpy.random.rand(len(biases)) )
errs_s = 0.1*numpy.array(aucs_s)


# Initialize CICalculator
c = CICalculator(dm,
    biases,
    aucs,
    errs,
    biases_s,
    aucs_s,
    errs_s
)

# Returns 0.530296... on my machine with random seed set to 0 at the beginning of the file.
print(c.calculate_ci())