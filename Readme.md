# Repository for CI calculation
This is the repository containing the code for the calculation of the confounding index, presented in *Ferrari, Elisa, Alessandra Retico, and Davide Bacciu. "Measuring the effects of confounders in medical supervised classification problems: the Confounding Index (CI)." Artificial intelligence in medicine 103 (2020): 101804.*

With respect to the description provided in the paper, in this code the matching part is not implemented and left to the user. The available code requires the user to provide a list of biases and AUCs obtained with the model they want to test.

The monotonicitiy condition is evaluated with the delta-monotonicity, as described in *Brooks, Martin, Yuhong Yan, and Daniel Lemire. "Scale-based monotonicity analysis in qualitative modelling with flat segments." (2005)*. 
If this function is used, the user is required to supply a threshold value, below which changes in the AUCs are not considered for the monotonicity evaluation.
The user may re-define the monotonicity evaluation as they please.

The code includes for files:

* `CI.py`: the functional code is entirely contained in this file.

* `delta_monotonicity_test.py`: this file contains the tests for the delta monotonicity evaluator.

* `ci_test.py`: this file contains the tests for the CI calculations.

* `sample_usage`: this file contains examples on how to use the CI calculator.

[![License: CC BY-NC 4.0](https://licensebuttons.net/l/by-nc/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc/4.0/)
The code is released under the CC-BY-NC 4.0 license. Users are free to use and re-distribute the code for non-commercial projects in any way they like, providing attribution by citing the paper:  *Ferrari, Elisa, Alessandra Retico, and Davide Bacciu. "Measuring the effects of confounders in medical supervised classification problems: the Confounding Index (CI)." Artificial intelligence in medicine 103 (2020): 101804.*
